package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.ChangePasswordPage;
import main.java.GlobalAccess;
import main.java.LoginPage;

public class ChangePasswordPageTest {

	LoginPage login;
	ChangePasswordPage changePass;
	WebDriver driver;

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		login = new LoginPage();
		changePass = new ChangePasswordPage();
		driver.get(GlobalAccess.baseURL);
	}

	@Test(priority = 1)
	public void checkValidEmail_Success() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy1");
		login.clickForSignin();
		Thread.sleep(3000);
	}

	@Test(priority = 2)
	public void checkWrondtPasswodInCurrentPasswordField() throws Throwable {
		changePass.profile_icon = driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img"));
		changePass.clickProfileIcon();
		Thread.sleep(3000);
		changePass.change_pass_click = driver.findElement(By.linkText("Change Password"));
		changePass.clickChangePass();
		Thread.sleep(1000);
		changePass.current_pass = driver.findElement(By.id("currentPassword"));
		changePass.enterCurrentPass("abc123");
		Thread.sleep(1000);
		changePass.new_pass = driver.findElement(By.id("newPassword"));
		changePass.enterNewPass("veryeasy1");
		Thread.sleep(1000);
		changePass.conf_pass = driver.findElement(By.id("confirmNewPassword"));
		changePass.enterConfPass("veryeasy1");
		Thread.sleep(1000);
		changePass.submit = driver.findElement(By.cssSelector("#create-user-form > div.m-portlet__foot.m-portlet__foot--fit > div > div > div > button"));
		changePass.submit().click();
		Thread.sleep(7000);
	}
	
	@Test(priority = 3)
	public void checkCorrecttPasswodInCurrentPasswordField() throws Throwable, StaleElementReferenceException, ElementNotVisibleException {
		changePass.current_pass.clear();
		changePass.new_pass.clear();
		changePass.conf_pass.clear();
		Thread.sleep(2000);
		changePass.current_pass = driver.findElement(By.id("currentPassword"));
		changePass.enterCurrentPass("veryeasy1");
		Thread.sleep(1000);
		changePass.new_pass = driver.findElement(By.id("newPassword"));
		changePass.enterNewPass("veryeasy11");
		Thread.sleep(1000);
		changePass.conf_pass = driver.findElement(By.id("confirmNewPassword"));
		changePass.enterConfPass("veryeasy11");
		Thread.sleep(2000);
		changePass.submit = driver.findElement(By.xpath("//*[@id='create-user-form']/div[2]/div/div/div/button"));
//		changePass.submit = driver.findElement(By.cssSelector("#create-user-form > div.m-portlet__foot.m-portlet__foot--fit > div > div > div > button"));
		Actions actions = new Actions(driver);
		actions.moveToElement(changePass.submit);
		actions.click().perform();
	}
	
	@Test(priority=4)
	public void checkChangedPassword() throws Throwable {
		new WebDriverWait(driver, 50).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='m_login']/div/div/div[2]/form/div[1]/input")));
		login.email = driver.findElement(By.xpath("//*[@id='m_login']/div/div/div[2]/form/div[1]/input"));
		login.pass = driver.findElement(By.xpath("//*[@id='m_login']/div/div/div[2]/form/div[2]/input"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy11");
		Thread.sleep(3000);
		login.clickForSignin();
	}
	

	@AfterClass
	public void closeDriver() throws Throwable {
		Thread.sleep(6000);
		driver.close();
	}

}
