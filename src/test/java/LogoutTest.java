package test.java;

import java.io.File;
import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.GlobalAccess;
import main.java.LoginPage;
import main.java.Logout;

public class LogoutTest {

	LoginPage login;
	Logout logout;
	WebDriver driver;

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		login = new LoginPage();
		driver.get(GlobalAccess.baseURL);
	}

	@Test(priority = 1)
	public void login_Success() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy1");
		login.clickForSignin();
		Thread.sleep(4000);
		// TakesScreenshot scrShot = ((TakesScreenshot) driver);
		// File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		// File DestFile = new File("/home/pravin/Pictures/screenshot.png");
		// FileUtils.copyFile(SrcFile, DestFile);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("/home/pravin/Pictures/screenshot1.png"));

	}

	@Test(priority = 2)
	public void logoutWithNo_success() throws Throwable {
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img")).click();
		Thread.sleep(2000);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("/home/pravin/Pictures/screenshot2.png"));
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.linkText("No")).click();
		;
		Thread.sleep(5000);
	}

	@Test(priority = 3)
	public void logoutWithYes_success() throws Throwable {
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.linkText("Yes")).click();
	}

	@AfterClass
	public void closeDriver() throws Throwable {
		Thread.sleep(5000);
		driver.close();
	}

}
