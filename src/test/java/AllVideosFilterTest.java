package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.AllVideosFilter;
import main.java.GlobalAccess;
import main.java.LoginPage;

public class AllVideosFilterTest {

	LoginPage login;
	AllVideosFilter videosFilter;
	WebDriver driver;
	String tempURL = GlobalAccess.baseURL + "/videos/explorer";

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		login = new LoginPage();
		videosFilter = new AllVideosFilter();
		driver.get(GlobalAccess.baseURL);
	}

	@Test(priority = 1)
	public void login_Success() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy1");
		login.clickForSignin();
		Thread.sleep(4000);
	}

	@Test(priority = 2)
	public void selectItem() throws Throwable {
		driver.navigate().to(tempURL);
		Thread.sleep(3000);
		videosFilter.selectFilter = driver.findElement(By.xpath("//*[@id='filterDropdown']"));
		videosFilter.clickSelectFilter();
		Thread.sleep(2000);
		videosFilter.publish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.unpublish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[2]"));
		videosFilter.checkForUnpublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		
		videosFilter.publish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		
		videosFilter.publish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.unpublish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[2]"));
		videosFilter.checkForUnpublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		videosFilter.clickSelectFilter();
	}
	
	@AfterClass
	public void closeDriver() throws Throwable {
		Thread.sleep(5000);
		driver.close();
	}

}
