package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.GlobalAccess;
import main.java.LoginPage;
import main.java.ManageUsersPage;

public class ManageUsersPageTest {

	WebDriver driver;
	LoginPage login;
	ManageUsersPage manageUser;

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		Reporter.log("Chrome Driver opened");
		driver.manage().window().maximize();
		login = new LoginPage();
		manageUser = new ManageUsersPage();
		driver.get(GlobalAccess.baseURL);
		Reporter.log("Got url from driver as http://prod.simultv.enveu.com");
	}

	@Test(priority = 1)
	public void login_Success() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy1");
		login.clickForSignin();
		Thread.sleep(4000);

	}

	@Test(priority = 2)
	public void publish() throws Throwable, WebDriverException {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.className("m-portlet__body")))
				.moveToElement(driver.findElement(By.className("m-datatable__table")))
				.moveToElement(driver.findElement(By.className("mCSB_container")))
				.moveToElement(driver.findElement(By.className("mCSB_outside")))
				.moveToElement(driver.findElement(By.className("m-datatable__row--even")))
				.moveToElement(driver.findElement(By.className("m-datatable__cell")))
				.moveToElement(driver.findElement(By.className("m--margin-right-10 ")))
				.moveToElement(driver.findElement(By.className("iconSize"))).click().build().perform();
		Thread.sleep(2000);
		// driver.findElement(By.cssSelector("#activateDeactivateModal > div > div > div.modal-footer > button.btn.m-btn--pill.btn-outline-success.btn-sm")).click();
		driver.findElement(By.xpath("//*[@id='activateDeactivateModal']/div/div/div[3]/button[1]")).click();
		Thread.sleep(4000);
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.className("m-portlet__body")))
		.moveToElement(driver.findElement(By.className("m-datatable__table")))
		.moveToElement(driver.findElement(By.className("mCSB_container")))
		.moveToElement(driver.findElement(By.className("mCSB_outside")))
		.moveToElement(driver.findElement(By.className("m-datatable__row--even")))
		.moveToElement(driver.findElement(By.className("m-datatable__cell")))
		.moveToElement(driver.findElement(By.className("m--margin-right-10 ")))
		.moveToElement(driver.findElement(By.className("iconSize"))).click().build().perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='activateDeactivateModal']/div/div/div[3]/button[1]")).click();
		Thread.sleep(4000);
	}

	 @AfterClass
	 public void closeDriver() throws Throwable {
	 Thread.sleep(4000);
	 driver.close();
	 }

}
