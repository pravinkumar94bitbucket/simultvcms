package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.ForgotPasswordPage;
import main.java.GlobalAccess;

public class ForgotPasswordPageTest {

	ForgotPasswordPage fpass;
	WebDriver driver;

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		fpass = new ForgotPasswordPage();
		driver.get(GlobalAccess.baseURL);
	}
	
	@Test(priority = 1)
	public void checkBlankEmail_Failure() throws Throwable {
		fpass.forgot_pass_link = driver.findElement(By.linkText("Forgot Password ?"));
		fpass.clickLinkForForgotPassword();
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.enterEmail("");
		fpass.clickForRequest();
		Thread.sleep(3000);
		fpass.clearMEmail();
	}

	@Test(priority = 2)
	public void checkInvalidEmail_Failure() throws Throwable {
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.enterEmail("abcnsddsjhyy8");
		fpass.clickForRequest();
		Thread.sleep(3000);
		fpass.clearMEmail();
	}

	@Test(priority = 3)
	public void checkNotExistingEmail_Failure() throws Throwable {
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.enterEmail("test123@gmail.com");
		fpass.clickForRequest();
		Thread.sleep(3000);
		fpass.clearMEmail();
	}

	@Test(priority = 4)
	public void checkCancel_Success() throws Throwable {
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.cancel = driver.findElement(By.xpath("//*[@id='m_login_forget_password_cancel']"));
		fpass.enterEmail("simultvbrand@gmail.com");
		fpass.clickForCancel();
		Thread.sleep(3000);
	}

	@Test(priority = 5)
	public void checkExisitingEmail_Success() throws Throwable {
		fpass.forgot_pass_link = driver.findElement(By.linkText("Forgot Password ?"));
		fpass.clickLinkForForgotPassword();
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.clearMEmail();
		fpass.enterEmail("simultvdemo@gmail.com");
		fpass.clickForRequest();
		Thread.sleep(3000);
	}

	@AfterClass
	public void closeDriver() {
		driver.close();
	}

}
