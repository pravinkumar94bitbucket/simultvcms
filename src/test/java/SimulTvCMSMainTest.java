package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import main.java.AllChannelsFilter;
import main.java.AllVideosFilter;
import main.java.ChangePasswordPage;
import main.java.ForgotPasswordPage;
import main.java.GlobalAccess;
import main.java.LoginPage;
import main.java.Logout;
import main.java.ManageUsersPage;

public class SimulTvCMSMainTest {

	LoginPage login;
	ManageUsersPage manageUsers;
	AllVideosFilter videosFilter;
	AllChannelsFilter channelsFilter;
	ChangePasswordPage changePass;
	Logout logout;
	ForgotPasswordPage fpass;
	WebDriver driver;
	String tempURL1 = GlobalAccess.baseURL + "/login?returnUrl=%2Fusers%2Fmanage";
	String tempURL2 = GlobalAccess.baseURL + "/videos/explorer";
	String tempURL3 = GlobalAccess.baseURL + "/channel/explorer";
	String tempURL4 = GlobalAccess.baseURL + "/videos/explorer";

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
//		 System.setProperty("webdriver.gecko.driver", "/home/pravin/geckodriver");
		driver = new ChromeDriver();
//		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		login = new LoginPage();
		manageUsers = new ManageUsersPage();
		videosFilter = new AllVideosFilter();
		channelsFilter = new AllChannelsFilter();
		changePass = new ChangePasswordPage();
		logout = new Logout();
		fpass = new ForgotPasswordPage();
		driver.get(GlobalAccess.baseURL);

		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
	}

	@Test(priority = 1)
	public void checkBlankEmail_Failure() throws Throwable {
		login.enterEmail("");
		login.enterPassword("");
		login.clickForSignin();
		Thread.sleep(3000);
		login.clearEmail();
		login.clearPassword();
	}

	@Test(priority = 2)
	public void checkInvalidEmail_Failure() throws Throwable {
		login.enterEmail("acdjsdh878979");
		login.enterPassword("      ");
		login.clickForSignin();
		Thread.sleep(3000);
		login.clearEmail();
		login.clearPassword();
	}

	@Test(priority = 3)
	public void checkNotExistingEmail_Failure() throws Throwable {
		login.enterEmail("abc123@gmail.com");
		login.enterPassword("test123");
		login.clickForSignin();
		Thread.sleep(6000);
		login.clearEmail();
		login.clearPassword();
	}

	@Test(priority = 4)
	public void checkExistingEmail_Success() throws Throwable {
		login.enterEmail("simultvdemo@gmail.com");
		login.enterPassword("veryeasy1");
		login.clickForSignin();
		Thread.sleep(4000);
	}

	@Test(priority = 5)
	public void publish() throws Throwable, WebDriverException {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.className("m-portlet__body")))
				.moveToElement(driver.findElement(By.className("m-datatable__table")))
				.moveToElement(driver.findElement(By.className("mCSB_container")))
				.moveToElement(driver.findElement(By.className("mCSB_outside")))
				.moveToElement(driver.findElement(By.className("m-datatable__row--even")))
				.moveToElement(driver.findElement(By.className("m-datatable__cell")))
				.moveToElement(driver.findElement(By.xpath("//*[@id='mCSB_1_container']/tr[1]/td[5]/span")))
				.moveToElement(driver.findElement(By.className("m--margin-right-10")))
				.moveToElement(driver.findElement(By.className("iconSize"))).click().build().perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='activateDeactivateModal']/div/div/div[3]/button[1]")).click();
		Thread.sleep(8000);
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.className("m-portlet__body")))
		.moveToElement(driver.findElement(By.className("m-datatable__table")))
		.moveToElement(driver.findElement(By.className("mCSB_container")))
		.moveToElement(driver.findElement(By.className("mCSB_outside")))
		.moveToElement(driver.findElement(By.className("m-datatable__row--even")))
		.moveToElement(driver.findElement(By.className("m-datatable__cell")))
//		.moveToElement(driver.findElement(By.xpath("//*[@id='mCSB_1_container']/tr[1]/td[5]/span")))
		.moveToElement(driver.findElement(By.className("m--margin-right-10 ")))
		.moveToElement(driver.findElement(By.className("iconSize"))).click().build().perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='activateDeactivateModal']/div/div/div[3]/button[1]")).click();
		driver.findElement(By.xpath("//*[@id='videoDuration']")).sendKeys("60");
		Thread.sleep(8000);
	}
	
	@Test(priority = 6)
	public void createNewVideo() throws Throwable {
		driver.navigate().to(tempURL1);
		Thread.sleep(8000);
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[1]/div/a")).click();
		driver.findElement(By.xpath("//*[@id='asset-title']")).sendKeys("Test Demo 1");
		driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[2]/div/textarea"))
				.sendKeys("This is test for description");
		Thread.sleep(3000);
		WebElement images = driver.findElement(By.id("fileupload"));
		images.sendKeys("/home/pravin/Pictures/myfile.jpg");
		Thread.sleep(3000);
		WebElement videos = driver.findElement(By.id("file2"));
		videos.sendKeys("/home/pravin/Videos/WildAtlanticWay10SecondAd.mp4");
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#file-upload-container > span > button:nth-child(2)")).click();
		Thread.sleep(8000);
		driver.findElement(By.id("videoDuration")).sendKeys("60");
		driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[9]/div/label/span")).click();
		driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[10]/div/div/button")).click();
		Thread.sleep(3000);
	}

	@Test(priority = 7)
	public void selectItemForVideos() throws Throwable {
		driver.navigate().to(tempURL2);
		Thread.sleep(3000);
		videosFilter.selectFilter = driver.findElement(By.xpath("//*[@id='filterDropdown']"));
		videosFilter.clickSelectFilter();
		Thread.sleep(2000);
		videosFilter.publish = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.unpublish = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[2]"));
		videosFilter.checkForUnpublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);

		videosFilter.publish = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);

		videosFilter.publish = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.unpublish = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[2]"));
		videosFilter.checkForUnpublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		videosFilter.clickSelectFilter();
		Thread.sleep(2000);
	}
	
	@Test(priority = 8)
	public void selectItemForChannels() throws Throwable {
		driver.navigate().to(tempURL3);
		Thread.sleep(3000);
		videosFilter.selectFilter = driver.findElement(By.xpath("//*[@id='filterDropdown']"));
		videosFilter.clickSelectFilter();
		Thread.sleep(2000);
		videosFilter.publish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.unpublish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[2]"));
		videosFilter.checkForUnpublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		
		videosFilter.publish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		
		videosFilter.publish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[1]"));
		videosFilter.checkForPublish();
		Thread.sleep(1000);
		videosFilter.unpublish = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/label[2]"));
		videosFilter.checkForUnpublish();
		Thread.sleep(1000);
		videosFilter.apply_filter = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/button"));
		videosFilter.applyFilter();
		Thread.sleep(4000);
		videosFilter.clickSelectFilter();
		Thread.sleep(2000);
	}

	@Test(priority = 9)
	public void checkWrongPasswodInCurrentPasswordField() throws Throwable {
		changePass.profile_icon = driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img"));
		changePass.clickProfileIcon();
		Thread.sleep(3000);
		changePass.change_pass_click = driver.findElement(By.linkText("Change Password"));
		changePass.clickChangePass();
		Thread.sleep(1000);
		changePass.current_pass = driver.findElement(By.id("currentPassword"));
		changePass.enterCurrentPass("abc123");
		Thread.sleep(1000);
		changePass.new_pass = driver.findElement(By.id("newPassword"));
		changePass.enterNewPass("veryeasy1");
		Thread.sleep(1000);
		changePass.conf_pass = driver.findElement(By.id("confirmNewPassword"));
		changePass.enterConfPass("veryeasy1");
		Thread.sleep(1000);
		changePass.submit = driver.findElement(By.cssSelector(
				"#create-user-form > div.m-portlet__foot.m-portlet__foot--fit > div > div > div > button"));
		changePass.submit().click();
		Thread.sleep(7000);
	}

	@Test(priority = 10)
	public void checkCorrecttPasswodInCurrentPasswordField()
			throws Throwable, StaleElementReferenceException, ElementNotVisibleException {
		changePass.current_pass.clear();
		changePass.new_pass.clear();
		changePass.conf_pass.clear();
		Thread.sleep(2000);
		changePass.current_pass = driver.findElement(By.id("currentPassword"));
		changePass.enterCurrentPass("veryeasy1");
		Thread.sleep(1000);
		changePass.new_pass = driver.findElement(By.id("newPassword"));
		changePass.enterNewPass("veryeasy1");
		Thread.sleep(1000);
		changePass.conf_pass = driver.findElement(By.id("confirmNewPassword"));
		changePass.enterConfPass("veryeasy1");
		Thread.sleep(2000);
		changePass.submit = driver.findElement(By.xpath("//*[@id='create-user-form']/div[2]/div/div/div/button"));
		// changePass.submit =
		// driver.findElement(By.cssSelector("#create-user-form >
		// div.m-portlet__foot.m-portlet__foot--fit > div > div > div >
		// button"));
		Actions actions = new Actions(driver);
		actions.moveToElement(changePass.submit);
		actions.click().perform();
	}

	@Test(priority = 11)
	public void checkChangedPassword() throws Throwable {
		new WebDriverWait(driver, 50).until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[@id='m_login']/div/div/div[2]/form/div[1]/input")));
		login.email = driver.findElement(By.xpath("//*[@id='m_login']/div/div/div[2]/form/div[1]/input"));
		login.pass = driver.findElement(By.xpath("//*[@id='m_login']/div/div/div[2]/form/div[2]/input"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy1");
		Thread.sleep(3000);
		login.clickForSignin();
		Thread.sleep(4000);
	}

	@Test(priority = 12)
	public void logoutWithNo_success() throws Throwable {
		logout.profile_icon = driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img"));
		logout.clickProfileIcon();
		Thread.sleep(2000);
		logout.logout_btn = driver
				.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a"));
		logout.clickLogoutButton();
		Thread.sleep(2000);
		logout.conf_no = driver.findElement(By.linkText("No"));
		logout.clickConfNo();
		Thread.sleep(3000);
	}

	@Test(priority = 13)
	public void logoutWithYes_success() throws Throwable {
		logout.profile_icon = driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img"));
		logout.clickProfileIcon();
		Thread.sleep(2000);
		logout.logout_btn = driver
				.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a"));
		logout.clickLogoutButton();
		Thread.sleep(2000);
		logout.conf_yes = driver.findElement(By.linkText("Yes"));
		logout.clickConfYes();

	}

	@Test(priority = 14)
	public void checkBlankFEmail_Failure() throws Throwable {
		driver.navigate().to(tempURL1);
		fpass.forgot_pass_link = driver.findElement(By.linkText("Forgot Password ?"));
		fpass.clickLinkForForgotPassword();
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.enterEmail("");
		fpass.clickForRequest();
		Thread.sleep(3000);
		fpass.clearMEmail();
	}

	@Test(priority = 15)
	public void checkInvalidFEmail_Failure() throws Throwable {
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.enterEmail("abcnsddsjhyy8");
		fpass.clickForRequest();
		Thread.sleep(3000);
		fpass.clearMEmail();
	}

	@Test(priority = 16)
	public void checkNotExistingFEmail_Failure() throws Throwable {
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.enterEmail("test123@gmail.com");
		fpass.clickForRequest();
		Thread.sleep(3000);
		fpass.clearMEmail();
	}

	@Test(priority = 17)
	public void checkCancel_Success() throws Throwable {
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.cancel = driver.findElement(By.xpath("//*[@id='m_login_forget_password_cancel']"));
		fpass.enterEmail("simultvbrand@gmail.com");
		fpass.clickForCancel();
		Thread.sleep(3000);
	}

	@Test(priority = 18)
	public void checkExisitingEmail_Success() throws Throwable {
		fpass.forgot_pass_link = driver.findElement(By.linkText("Forgot Password ?"));
		fpass.clickLinkForForgotPassword();
		fpass.m_email = driver.findElement(By.id("m_email"));
		fpass.request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		fpass.clearMEmail();
		fpass.enterEmail("simultvdemo@gmail.com");
		fpass.clickForRequest();
		Thread.sleep(3000);
	}

	@AfterClass
	public void closeDriver() throws Throwable {
		Thread.sleep(5000);
		driver.close();
	}

}
