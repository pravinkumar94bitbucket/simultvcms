package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.GlobalAccess;
import main.java.LoginPage;

public class LoginPageTest {

	LoginPage login;
	WebDriver driver;

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		login = new LoginPage();
		driver.get(GlobalAccess.baseURL);
	}

	@Test(priority = 1)
	public void checkBlankEmail_Failure() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.enterEmail("");
		login.enterPassword("");
		login.clickForSignin();
		Thread.sleep(3000);
		login.clearEmail();
		login.clearPassword();
	}
	
	@Test(priority = 2)
	public void checkInvalidEmail_Failure() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.enterEmail("acdjsdh878979");
		login.enterPassword("      ");
		login.clickForSignin();
		Thread.sleep(3000);
		login.clearEmail();
		login.clearPassword();
	}

	@Test(priority = 3)
	public void checkNotExistingEmail_Failure() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.enterEmail("abc123@gmail.com");
		login.enterPassword("test123");
		login.clickForSignin();
		Thread.sleep(6000);
		login.clearEmail();
		login.clearPassword();
	}

	@Test(priority = 4)
	public void checkExistingEmail_Success() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.enterEmail("simultvdemo@gmail.com");
		login.enterPassword("veryeasy1");
		login.clickForSignin();
		Thread.sleep(4000);
	}

	@AfterClass
	public void closeDriver() {
		driver.close();
	}

}
