package test.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.CreateNewVideo;
import main.java.GlobalAccess;
import main.java.LoginPage;

public class CreateNewVideoTest {

	LoginPage login;
	CreateNewVideo createVideo;
	WebDriver driver;
	String tempURL1 = GlobalAccess.baseURL + "/videos/explorer";

	@BeforeClass
	public void addSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		login = new LoginPage();
		createVideo = new CreateNewVideo();
		driver.get(GlobalAccess.baseURL);
	}

	@Test(priority = 1)
	public void login_Success() throws Throwable {
		login.email = driver.findElement(By.name("email"));
		login.pass = driver.findElement(By.name("password"));
		login.signin = driver.findElement(By.id("m_login_signin_submit"));
		login.email.sendKeys("simultvdemo@gmail.com");
		login.pass.sendKeys("veryeasy1");
		login.clickForSignin();
		Thread.sleep(6000);
	}

	// @Test(priority = 2)
	// public void createNewVideo1() throws Throwable {
	// driver.navigate().to(tempURL1);
	// Thread.sleep(8000);
	// driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[1]/div/a")).click();
	// driver.findElement(By.xpath("//*[@id='asset-title']")).sendKeys("Test
	// Demo 2");
	// driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[2]/div/textarea"))
	// .sendKeys("This is test for description");
	// Thread.sleep(3000);
	// WebElement images = driver.findElement(By.id("fileupload"));
	// images.sendKeys("/home/pravin/Pictures/myfile.jpg");
	// Thread.sleep(3000);
	// WebElement videos = driver.findElement(By.id("file2"));
	// videos.sendKeys("/home/pravin/Videos/WildAtlanticWay10SecondAd.mp4");
	// Thread.sleep(3000);
	// driver.findElement(By.cssSelector("#file-upload-container > span >
	// button:nth-child(2)")).click();
	// Thread.sleep(8000);
	// driver.findElement(By.id("videoDuration")).sendKeys("60");
	// driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[9]/div/label/span")).click();
	// driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[10]/div/div/button")).click();
	// Thread.sleep(3000);
	// }

	@Test(priority = 2)
	public void createNewVideo2() throws Throwable {
		driver.navigate().to(tempURL1);
		Thread.sleep(8000);
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[1]/div/a")).click();
		driver.findElement(By.xpath("//*[@id='asset-title']")).sendKeys("Test Demo 3");
		driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[2]/div/textarea"))
				.sendKeys("This is test for description");
		Thread.sleep(3000);
		driver.findElement(By.id("fileupload")).click();
		String filepath = "/home/pravin/Pictures/";
		String inputFilePath = "/home/pravin/Pictures/";
		Screen s = new Screen();
//		Pattern fileInputTextBox = new Pattern(filepath + "test.png");
		Pattern openButton = new Pattern(filepath + "myfile.jpg");
		s.wait(openButton, 20);
		s.type(openButton, inputFilePath + "sapne.jpg");
		s.click(openButton);
		Thread.sleep(3000);
		WebElement videos = driver.findElement(By.id("file2"));
		videos.sendKeys("/home/pravin/Videos/WildAtlanticWay10SecondAd.mp4");
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#file-upload-container > span > button:nth-child(2)")).click();
		Thread.sleep(8000);
		driver.findElement(By.id("videoDuration")).sendKeys("60");
		driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[9]/div/label/span")).click();
		driver.findElement(By.xpath("//*[@id='create-video-asset-form']/div[10]/div/div/button")).click();
		Thread.sleep(3000);
	}

	@AfterClass
	public void closeDriver() throws Throwable {
		Thread.sleep(4000);
		// driver.close();
	}
}
