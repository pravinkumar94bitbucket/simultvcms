package main.java;

import org.openqa.selenium.WebElement;

public class Logout {

	public WebElement profile_icon;

	public WebElement logout_btn;

	public WebElement conf_no;

	public WebElement conf_yes;

	public WebElement clickProfileIcon() {
		this.profile_icon.click();
		return this.profile_icon;
	}

	public WebElement clickLogoutButton() {
		this.logout_btn.click();
		return this.logout_btn;
	}

	public WebElement clickConfNo() {
		this.conf_no.click();
		return this.conf_no;
	}

	public WebElement clickConfYes() {
		this.conf_yes.click();
		return this.conf_yes;
	}
}
