package main.java;

import org.openqa.selenium.WebElement;

public class AllChannelsFilter {
	
	public WebElement selectFilter;
	
	public WebElement publish;
	
	public WebElement unpublish;
	
	public WebElement apply_filter;
	
	public WebElement clickSelectFilter() {
		this.selectFilter.click();
		return this.selectFilter;
	}
	
	public WebElement checkForPublish() {
		this.publish.click();
		return this.publish;
	}
	
	public WebElement checkForUnpublish() {
		this.unpublish.click();
		return this.unpublish;
	}
	
	public WebElement applyFilter() {
		this.apply_filter.click();
		return this.apply_filter;
	}

}
