package main.java;


import org.openqa.selenium.WebElement;

public class LoginPage {
	
	public WebElement email;
	
	public WebElement pass;
	
	public WebElement signin;
	
	public void enterEmail(String enterEmail) {
		this.email.sendKeys(enterEmail);
	}
	
	public void enterPassword(String enterPassword) {
		this.pass.sendKeys(enterPassword);
	}
	
	public void clearEmail() {
		this.email.clear();
	}
	
	public void clearPassword() {
		this.pass.clear();
	}
	
	public WebElement clickForSignin() {
		this.signin.click();
		return this.signin;
	}

}
