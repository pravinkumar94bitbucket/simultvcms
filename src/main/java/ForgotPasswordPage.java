package main.java;

import org.openqa.selenium.WebElement;

public class ForgotPasswordPage {

	public WebElement forgot_pass_link;

	public WebElement m_email;

	public WebElement request;

	public WebElement cancel;

	public WebElement clickLinkForForgotPassword() {
		this.forgot_pass_link.click();
		return this.forgot_pass_link;
	}
	
	public void enterEmail(String enterEmail) {
		this.m_email.sendKeys(enterEmail);
	}

	public WebElement clearMEmail() {
		this.m_email.clear();
		return this.m_email;
	}

	public WebElement clickForRequest() {
		this.request.click();
		return this.request;
	}

	public WebElement clickForCancel() {
		this.cancel.click();
		return this.cancel;
	}
}
