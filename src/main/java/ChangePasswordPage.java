package main.java;

import org.openqa.selenium.WebElement;

public class ChangePasswordPage {

	public WebElement profile_icon;

	public WebElement change_pass_click;

	public WebElement current_pass;

	public WebElement new_pass;

	public WebElement conf_pass;

	public WebElement submit;

	public WebElement clickProfileIcon() {
		this.profile_icon.click();
		return this.profile_icon;
	}

	public WebElement clickChangePass() {
		this.change_pass_click.click();
		return this.change_pass_click;
	}

	public void enterCurrentPass(String currentPassword) {
		this.current_pass.sendKeys(currentPassword);
	}

	public void enterNewPass(String newPassword) {
		this.new_pass.sendKeys(newPassword);
	}

	public void enterConfPass(String confPassword) {
		this.conf_pass.sendKeys(confPassword);
	}

	public WebElement submit() {
		this.submit.click();
		return this.submit;
	}

}
