package main.java;

import org.openqa.selenium.WebElement;

public class ManageUsersPage {

	public WebElement publish;

	public WebElement unpublish;

	public void clickForPublish() {
		this.publish.click();
	}

	public void cickForUnpublish() {
		this.unpublish.click();
	}

}
