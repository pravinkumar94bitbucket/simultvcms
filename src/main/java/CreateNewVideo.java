package main.java;

import org.openqa.selenium.WebElement;

public class CreateNewVideo {
	
	public WebElement clickCreateNewVideo;
	
	public WebElement enterTitile;
	
	public WebElement enterDescription;
	
	public WebElement thumbnailImage;
	
	public WebElement thumbnailVideo;
	
	public WebElement publishOptional;
	
	public WebElement submit;
	
	public void clickCreateNewVideos() {
		this.clickCreateNewVideo.click();
	}
	
	public void enterTitle(String title) {
		this.enterTitile.sendKeys(title);
	}
	
	public void enterDescriptions(String desc) {
		this.enterDescription.sendKeys(desc);
	}
	
	public void addImage(String image) {
		this.thumbnailImage.sendKeys(image);
	}
	
	public void addVideo(String video) {
		this.thumbnailVideo.sendKeys(video);
	}
	
	public WebElement clickSubmitButton() {
		this.submit.click();
		return this.submit;
	}

}
